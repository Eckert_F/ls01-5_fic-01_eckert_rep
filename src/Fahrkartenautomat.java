import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);
        float betragProTicket;
        float eingezahlterGesamtbetrag;
        float eingeworfeneMuenze;
        float rueckgabebetrag;
        float zuZahlenderBetrag = 0;
        int anzahlTickets = 0;

        System.out.println("Zu zahlender Betrag (EURO): ");
        betragProTicket = tastatur.nextFloat();
        System.out.println("Anzahl der Tickets;	");
        anzahlTickets = tastatur.nextInt();
        if(anzahlTickets > 0) {
            zuZahlenderBetrag = betragProTicket * anzahlTickets;
        }
        eingezahlterGesamtbetrag = 0.0f;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.printf("Noch zu zahlen: " + "%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.println(" Euro ");
            System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextFloat();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");

        rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rueckgabebetrag > 0.0)
        {
            System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabebetrag + " EURO");
            System.out.println("wird in folgenden Muenzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wuenschen Ihnen eine gute Fahrt.");
    }
}